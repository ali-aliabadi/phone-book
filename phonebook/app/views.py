from rest_framework import generics, mixins

from .serializers import PhoneBookSerializer, PersonSerializer, NumberSerializer
from .models import PhoneBook, Person, Number


class PhoneBookDetailApiView(
    generics.RetrieveAPIView,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin
):
    authentication_classes = []
    permission_classes = []
    serializer_class = PhoneBookSerializer
    queryset = PhoneBook.objects.all()
    lookup_field = 'id'

    def put(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


class PhoneBookListApiView(
    generics.ListAPIView,
    mixins.CreateModelMixin
):
    authentication_classes = []
    permission_classes = []
    serializer_class = PhoneBookSerializer

    def get_queryset(self):
        """
        user can search top 10 signal records in a specific symbol with ?q=symbol
        or get top n signals of all symbols with ?s=n
        """
        qs = PhoneBook.objects.all()
        s_query = self.request.GET.get('s', None)
        if s_query is not None:
            qs = PhoneBook.objects.all().order_by('-id')[:int(s_query) if int(s_query) >= 0 else -int(s_query)]
        return qs

    def post(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)


class PersonDetailApiView(
    generics.RetrieveAPIView,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin
):
    authentication_classes = []
    permission_classes = []
    serializer_class = PersonSerializer
    queryset = Person.objects.all()
    lookup_field = 'id'

    def put(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


class PersonListApiView(
    generics.ListAPIView,
    mixins.CreateModelMixin
):
    authentication_classes = []
    permission_classes = []
    serializer_class = PersonSerializer

    def get_queryset(self):
        """
        user can search top 10 signal records in a specific symbol with ?q=symbol
        or get top n signals of all symbols with ?s=n
        """
        qs = Person.objects.all()
        s_query = self.request.GET.get('s', None)
        if s_query is not None:
            qs = Person.objects.all().order_by('-id')[:int(s_query) if int(s_query) >= 0 else -int(s_query)]
        return qs

    def post(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)


class NumberDetailApiView(
    generics.RetrieveAPIView,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin
):
    authentication_classes = []
    permission_classes = []
    serializer_class = NumberSerializer
    queryset = Number.objects.all()
    lookup_field = 'id'

    def put(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


class NumberListApiView(
    generics.ListAPIView,
    mixins.CreateModelMixin
):
    authentication_classes = []
    permission_classes = []
    serializer_class = NumberSerializer

    def get_queryset(self):
        """
        user can search top 10 signal records in a specific symbol with ?q=symbol
        or get top n signals of all symbols with ?s=n
        """
        qs = Number.objects.all()
        s_query = self.request.GET.get('s', None)
        if s_query is not None:
            qs = Number.objects.all().order_by('-id')[:int(s_query) if int(s_query) >= 0 else -int(s_query)]
        return qs

    def post(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)
