from django.db import models

from .consts import ROLE_CHOICES


class PhoneBook(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class Person(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    city = models.CharField(max_length=32)
    postal_code = models.CharField(max_length=16)
    phone_book = models.ForeignKey(PhoneBook, on_delete=models.CASCADE, related_name='persons')

    def __str__(self):
        return '<name: {} {} phone_book_id: {}>'.format(self.first_name, self.last_name, self.phone_book)


class Number(models.Model):
    number = models.CharField(max_length=16)
    type = models.CharField(max_length=6, choices=ROLE_CHOICES)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='numbers')

    def __str__(self):
        return '<type: {}, number: {}, user_id: {}>'.format(self.type, self.number, self.person)
