from django.urls import path, re_path

from .views import PhoneBookListApiView, PhoneBookDetailApiView, PersonDetailApiView, PersonListApiView, \
    NumberDetailApiView, NumberListApiView

urlpatterns = [
    path('phonebook/', PhoneBookListApiView.as_view(), name='PhoneBookListApiView'),
    re_path(r'phonebook/(?P<id>\d+)/$', PhoneBookDetailApiView.as_view(), name='PhoneBookDetailApiView'),
    path('person/', PersonListApiView.as_view(), name='PersonListAPIView'),
    re_path(r'person/(?P<id>\d+)/$', PersonDetailApiView.as_view(), name='PersonDetailApiView'),
    path('number/', NumberListApiView.as_view(), name='NumberListAPIView'),
    re_path(r'number/(?P<id>\d+)/$', NumberDetailApiView.as_view(), name='NumberDetailApiView')
]
