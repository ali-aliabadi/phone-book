import json

from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status

from .models import PhoneBook
from .serializers import PhoneBookSerializer

client = Client()


class PhoneBookTest(TestCase):

    def setUp(self) -> None:
        PhoneBook.objects.create(name='pb')

    def test_phone_book_str(self):
        pb = PhoneBook.objects.get(name='pb')
        self.assertEqual(str(pb), 'pb')


class GetAllPhoneBooksTest(TestCase):

    def setUp(self):
        PhoneBook.objects.create(name='one')
        PhoneBook.objects.create(name='two')

    def test_get_all_phone_books(self):
        response = client.get(reverse('PhoneBookListApiView'))
        pbs = PhoneBook.objects.all()
        serializer = PhoneBookSerializer(pbs, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSinglePhoneBookTest(TestCase):

    def setUp(self):
        self.one = PhoneBook.objects.create(name='one')
        self.two = PhoneBook.objects.create(name='two')

    def test_get_valid_single_phone_book(self):
        response = client.get(reverse('PhoneBookDetailApiView', kwargs={'id': self.one.pk}))
        pb = PhoneBook.objects.get(pk=self.one.pk)
        serializer = PhoneBookSerializer(pb)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_phone_book(self):
        response = client.get(reverse('PhoneBookDetailApiView', kwargs={'id': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewPhoneBookTest(TestCase):

    def setUp(self):
        self.valid_payload = {
            'name': 'one'
        }
        self.invalid_payload = {
            'title': 'two',
        }

    def test_create_valid_phone_book(self):
        response = client.post(
            reverse('PhoneBookListApiView'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_phone_book(self):
        response = client.post(
            reverse('PhoneBookListApiView'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateSinglePhoneBookTest(TestCase):

    def setUp(self):
        self.one = PhoneBook.objects.create(name='one')
        self.valid_payload = {
            'name': 'new_one'
        }
        self.invalid_payload = {
            'title': 'new_two'
        }

    def test_valid_update_phone_book(self):
        response = client.put(
            reverse('PhoneBookDetailApiView', kwargs={'id': self.one.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_phone_book(self):
        response = client.put(
            reverse('PhoneBookDetailApiView', kwargs={'id': self.one.pk}),
            data=json.dumps(self.invalid_payload),
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeleteSinglePhoneBookTest(TestCase):

    def setUp(self):
        self.one = PhoneBook.objects.create(name='one')

    def test_valid_delete_phone_book(self):
        response = client.delete(
            reverse('PhoneBookDetailApiView', kwargs={'id': self.one.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_phone_book(self):
        response = client.delete(
            reverse('PhoneBookDetailApiView', kwargs={'id': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
