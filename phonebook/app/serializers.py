from rest_framework import serializers

from .models import PhoneBook, Person, Number


class NumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Number
        fields = ['id', 'person', 'type', 'number']


class PersonSerializer(serializers.ModelSerializer):
    numbers = NumberSerializer(many=True, read_only=True)

    class Meta:
        model = Person
        fields = ['id', 'phone_book', 'first_name', 'last_name', 'city', 'postal_code', 'numbers']


class PhoneBookSerializer(serializers.ModelSerializer):
    persons = PersonSerializer(many=True, read_only=True)

    class Meta:
        model = PhoneBook
        fields = ['id', 'name', 'persons']
