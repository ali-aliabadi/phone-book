from django.contrib import admin

from .models import PhoneBook, Person, Number

admin.site.register(PhoneBook)
admin.site.register(Person)
admin.site.register(Number)
