![pylint](https://gitlab.com/ali-aliabadi/phone-book/-/jobs/artifacts/master/raw/pylint/pylint.svg?job=pylint)   

## Phonebook

#### available urls

* :8080/app/phonebook/
* :8080/app/phonebook/<phonebook_id>/
* :8080/app/person/
* :8080/app/person/<person_id>/
* :8080/app/number/
* :8080/app/number/<number_id>/

### Run
```bash
docker-compose up
```

#### other
* make sure port 8080 is empty before running
* you can take backup of data by simply saving db.sqlit3
